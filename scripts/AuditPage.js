function refreshEvents(forcedate) {
    var date = $('#selectedDate').datepicker('getDate');
    
    if(forcedate==1){
        var data = {isDate: 1, selectedDate: $.datepicker.formatDate('mm-dd-yy', date)};
    } else {
        var data = {isDate: $(".eventsSelected").data("value"), selectedDate: $.datepicker.formatDate('mm-dd-yy', date)};
    }
    console.log(data);
    $("#eventsView").load("/ajax/eventList.cshtml", data);
}

$(function () {
    var currentdate = new Date();
    
    $("#selectedDate").datepicker({dateFormat: 'dd/mm/yy'});
    $("#selectedDate")[0].value=currentdate.getDate() + "/"+(currentdate.getMonth()+1) 
		    + "/" + currentdate.getFullYear();
    
	$(document).on("click", "#scheduleAudit", function (){
		showOverlay("/ajax/scheduleAudit.cshtml","");
	});
	
	$(document).on("click", ".event", function (){
		showOverlay("/ajax/view/viewEvent.cshtml",$(this).attr("eventID"));
	});
    
    $("#selectedDate").change(function() {
        refreshEvents(1);
    });
    
    $(document).on("click", ".eventsTypeBtn", function (){
        $(".eventsTypeBtn").removeClass("eventsSelected");
        $(this).addClass("eventsSelected");
        $(this).blur();
        
        refreshEvents();
    });
});
--This document is a sql script which generates a complete and ready to use eScorecard database in sql server.
--The following tables are Included.
/*
AuditFields                 --  done
Audits                      --  done
AuditTemplates              --  done
CompletedAuditFields        --  done
EmployeeMedia
Employees                   --  done
KPAMedia
PKPAs                       --  done
CKPAs                       --  done
StoreSuperLinks
RegionSuperLinks
CKPASuperLinks
PKPASuperLinks
KPISuperLinks
PKPATemplates               --  done
CKPATemplates               --  done
KPIMedia
KPIs                        --  done
KPITemplates                --  done
Nonconformities             --  done
Positions                   --  done
Regions                     --  done
SATemplateFields            --  done
PKPATemplateFields          --  done
CKPATemplateFields          --  done
ServiceAreaMedia
ServiceAreas                --  done
ServiceAreaTemplates        --  done
Stores                      --  done
*/

BEGIN TRANSACTION [CreateTbls]

BEGIN TRY

--*******************************TABLE DEFINITIONS*********************************************

--This table holds the data for all the positions in the application. All employees must hold a position.

CREATE TABLE Positions
(
    ID int IDENTITY(1,1) PRIMARY KEY,                       --ID column for this table.
    Title varchar(255) NOT NULL,                            --Title of this position.
    Super int REFERENCES Positions(ID) NOT NULL,            --The supervising position of this position. Self refferences for Admin.
    Description varchar(512),                               --The description of this position.
    AccountType int NOT NULL                                --The account type of this position. 1="Super Admin", 2="User Admin", 3="Standard User"
);

--The application needs the Admin Account to Run.
INSERT INTO Positions (Title, Super, Description, AccountType) VALUES ('Admin', 1, 'System Admin', 1);


--****************************************************************************************************

--This table holds the data for all the regions in the application. All stores must be in a region.

CREATE TABLE Regions
(
    ID int IDENTITY(1,1) PRIMARY KEY,                       --ID column for this table.
    Name varchar(255) NOT NULL,                             --Name of this region.
    Super int REFERENCES Employees(ID) NOT NULL,            --The supervising employee of this position.
    Location varchar(95),                                   --The description of this region.
    Description varchar(511),                              --The description of this region.
);


--****************************************************************************************************

--This table holds the data for all the stores in the application.

CREATE TABLE Stores
(
    ID int IDENTITY(1,1) PRIMARY KEY,                       --ID column for this table.
    Name varchar(255) NOT NULL,                             --Name of this Store.
    Region int REFERENCES Employees(ID) NOT NULL,           --The region to which this store belongs.
    Address varchar(95),                                    --The address of this store.
    ContactNumber varchar(10),                              --The contact number of this store.
    Email varchar(255),                                     --The store's email address.
    StoreNumber varchar(20),                                --The company assigned store number.
);


--****************************************************************************************************


--This table stores all the data of the employees in the application, including some personal information.

CREATE TABLE Employees
(
    ID int IDENTITY(1,1) PRIMARY KEY,                       --ID column for this table.
    Position int REFERENCES Positions(ID) NOT NULL,         --Position this employee holds.
    Super int REFERENCES Employees(ID) NOT NULL,            --The supervising employee of this employee. Self refferences for Admin.
    Store int REFERENCES Stores(ID),                        --The store to which this employee belongs
    LastName varchar(35) NOT NULL,                          --The last name of this employee.
    FirstName varchar(35) NOT NULL,                         --The first name of this employee.
    Username varchar(255) NOT NULL,                         --The username of this employee.
    Password varchar(128) NOT NULL,                         --The sha1(for now) hashed password of this employee.
    ContactNumber varchar(10),                              --The contact number of this employee.
    EmployeeNumber varchar(20),                             --The company assigned employee number.
    IDNumber varchar(26) NOT NULL,                          --The employee's national ID number.
    Email varchar(255),                                     --The employee's preffered email address.
);

--The application needs the Admin Account to Run.
INSERT INTO Employees (Position, Super, LastName, FirstName, Username, Password, IDNumber) VALUES (1, 1, 'Admin', 'Admin', 'Admin', '4e7afebcfbae000b22c7c85e5560f89a2a0280b4', '');
--Default password is Admin.


--****************************************************************************************************


--This table stores all the information of the service area types which are applied to stores.

CREATE TABLE ServiceAreaTemplates
(
    ID int IDENTITY(1,1) PRIMARY KEY,                       --ID column for this table.
    Name varchar(255) NOT NULL,                             --Name of this Service Area type.
    Policy varchar(511),                                   --The policy of this Service Area.
);


--****************************************************************************************************


--This table stores all the instances of service are templates to stores.

CREATE TABLE ServiceAreas
(
    ID int IDENTITY(1,1) PRIMARY KEY,                       --ID column for this table.
    Type int REFERENCES ServiceAreaTemplates(ID) NOT NULL,  --The service area template associated with the service area.
    Parent int REFERENCES Stores(ID) NOT NULL,              --The Store to which this service area belongs.
    Weighting DECIMAL(5,2) NOT NULL,                        --The weighting of the parent store which this service area provides.
);


--****************************************************************************************************


--This table stores all the information of the parent KPAs types which are applied to service areas.

CREATE TABLE PKPATemplates
(
    ID int IDENTITY(1,1) PRIMARY KEY,                       --ID column for this table.
    Name varchar(255) NOT NULL,                             --Name of this parent KPA type.
    Policy varchar(511),                                   --The policy of this PKPA.
);


--****************************************************************************************************


--This table stores all the instances of parent KPAs applied to service areas.

CREATE TABLE PKPAs
(
    ID int IDENTITY(1,1) PRIMARY KEY,                       --ID column for this table.
    Type int REFERENCES PKPATemplates(ID) NOT NULL,         --The parent KPA template associated with this PKPA.
    Parent int REFERENCES ServiceAreas(ID) NOT NULL,        --The service area to which this PKPA belongs.
    Weighting DECIMAL(5,2) NOT NULL,                        --The weighting of the parent service area which this PKPA provides.
);


--****************************************************************************************************


--This table stores all the information of the child KPAs types which are applied to service areas.

CREATE TABLE CKPATemplates
(
    ID int IDENTITY(1,1) PRIMARY KEY,                       --ID column for this table.
    Name varchar(255) NOT NULL,                             --Name of this CKPA
    Policy varchar(511),                                   --The policy of this CKPA.
);


--****************************************************************************************************


--This table stores all the instances of child KPAs applied to PKPAs.

CREATE TABLE CKPAs
(
    ID int IDENTITY(1,1) PRIMARY KEY,                       --ID column for this table.
    Type int REFERENCES CKPATemplates(ID) NOT NULL,         --The child KPA template associated with this CKPA.
    Parent int REFERENCES PKPAs(ID) NOT NULL,               --The PKPA to which this CKPA belongs.
    Weighting DECIMAL(5,2) NOT NULL,                        --The weighting of the parent PKPA which this CKPA provides.
);


--****************************************************************************************************


--This table stores all the information of the child KPAs types which are applied to service areas.

CREATE TABLE KPITemplates
(
    ID int IDENTITY(1,1) PRIMARY KEY,                       --ID column for this table.
    InputType int NOT NULL,                                 --The input type of this KPI. 1-Number Range, 2-Boolean, 3-Rating, 4-Time
    Input1 Decimal(9,3) NOT NULL,                           --Specifies the correct input for this KPI. Always used. 1 For true, 0 for false in boolean. Rating number. Range/Time minimum.
    Input2 Decimal(9,3),                                    --Specifies the correct input for this KPI. Only used for Range/Time maximum.
    Units varchar(10),                                      --Units for this KPI. Only used in number range and possibly time.
    Name varchar(255) NOT NULL,                             --Name of this KPI
    Description varchar(511),                              --The Description of this KPI.
);


--****************************************************************************************************


--This table stores all the instances of KPIs applied to CKPAs.

CREATE TABLE KPIs
(
    ID int IDENTITY(1,1) PRIMARY KEY,                       --ID column for this table.
    Type int REFERENCES KPITemplates(ID) NOT NULL,          --The child KPA template associated with this KPI.
    Parent int REFERENCES CKPAs(ID) NOT NULL,               --The CKPA to which this KPI belongs.
    Weighting DECIMAL(5,2) NOT NULL,                        --The weighting of the parent CKPA which this KPI provides.
);


--****************************************************************************************************


--This table stores all the PKPA Templates associated with a service area template.

CREATE TABLE SATemplateFields
(
    ID int IDENTITY(1,1) PRIMARY KEY,                       --ID column for this table.
    SATemplate int REFERENCES SATemplates(ID) NOT NULL,     --The Service Area Template to which this field belongs.
    PKPATemplate int REFERENCES PKPATemplates(ID) NOT NULL, --The Corresponding PKPA Template for this field.
    PKPAWeighting DECIMAL(5,2) NOT NULL                     --The Weighting of this PKPATemplate within this service area.
);


--****************************************************************************************************


--This table stores all the CKPA Templates associated with a PKPA Template.

CREATE TABLE PKPATemplateFields
(
    ID int IDENTITY(1,1) PRIMARY KEY,                       --ID column for this table.
    PKPATemplate int REFERENCES PKPATemplates(ID) NOT NULL, --The PKPA Template to which this field belongs.
    CKPATemplate int REFERENCES CKPATemplates(ID) NOT NULL, --The Corresponding CKPA Template for this field.
    CKPAWeighting DECIMAL(5,2) NOT NULL                     --The Weighting of this CKPATemplate within this PKPA Template.
);


--****************************************************************************************************


--This table stores all the KPI Templates associated with a CKPA Template.

CREATE TABLE CKPATemplateFields
(
    ID int IDENTITY(1,1) PRIMARY KEY,                       --ID column for this table.
    CKPATemplate int REFERENCES CKPATemplates(ID) NOT NULL, --The CKPA Template to which this field belongs.
    KPITemplate int REFERENCES KPITemplates(ID) NOT NULL,   --The Corresponding KPI Template for this field.
    KPIWeighting DECIMAL(5,2) NOT NULL                      --The Weighting of this KPI Template within this CKPA Template.
);


--****************************************************************************************************


--This table stores all the information of the audit types.

CREATE TABLE AuditTemplates
(
    ID int IDENTITY(1,1) PRIMARY KEY,                       --ID column for this table.
    Name varchar(255) NOT NULL,                             --Name of this audit template.
    DateCreated DATETIME NOT NULL,                          --Date and time of the creation of this audit template.
    CreatorID INT REFERENCES Employees(ID) NOT NULL,        --Creator of this audit template.
    isFollowUp BIT NOT NULL                                 --Boolean signifying whether this is a follow up audit.
);


--****************************************************************************************************


--This table stores all the fields in the audit templates.

CREATE TABLE AuditFields
(
    ID int IDENTITY(1,1) PRIMARY KEY,                       --ID column for this table.
    Audit INT REFERENCES Audits(ID) NOT NULL,               --Audit template to which this field belongs.
    ServiceArea INT REFERENCES ServiceAreaTemplates(ID) NOT NULL,   --Service Area to which this field belongs.
    ParentKPA INT REFERENCES PKPATemplates(ID) NOT NULL,    --Parent KPA to which this field belongs.
    ChildKPA INT REFERENCES CKPATemplates(ID) NOT NULL,     --Child KPA to which this field belongs.
);


--****************************************************************************************************


--This table stores all the fields in the audit templates.

CREATE TABLE Audits
(
    ID int IDENTITY(0,1) PRIMARY KEY,                       --ID column for this table.
    DateScheduled DATETIME NOT NULL,                        --Date and time for which this audit was scheduled.
    Type INT REFERENCES AuditTemplates(ID),                 --The audit type of this audit.
    Auditor INT REFERENCES Employees(ID) NOT NULL,          --The employee who is to perform this audit.
    Store INT REFERENCES Stores(ID),                        --The store which this audit is/was applicable.
    isCompleted BIT NOT NULL,                               --Boolean whether this audit has been completed.
    DateOfCompletion DATETIME                               --Actual date and time of completion
);

INSERT INTO Audits (DateScheduled,Auditor,isCompleted) VALUES ('19700101',1,0)      -- Dummy entry as to nullify the followup of some nonconformities.


--****************************************************************************************************


--This table stores all the fields completed by auditors.

CREATE TABLE CompletedAuditFields
(
    ID int IDENTITY(1,1) PRIMARY KEY,                       --ID column for this table.
    Audit INT REFERENCES Auidts(ID) NOT NULL,               --The audit to which this completed audit field belongs.
    Input DECIMAL(9,3) NOT NULL,                            --The Input provided by the auditor.
    Comment varchar(511),                                  --The comment provided by the auditor.
    KPI INT REFERENCES KPIs(ID) NOT NULL,                   --The audit type of this audit.
);


--****************************************************************************************************


--This table stores all nonconformities that occur in the audits.

CREATE TABLE Nonconformities
(
    ID int IDENTITY(1,1) PRIMARY KEY,                       --ID column for this table.
    CompletedAuditField INT REFERENCES CompletedAuditFields(ID) NOT NULL,   --The audit to which this completed audit field belongs. Audit ID signifies no follow up and a null field designates the next visit to this store.
    ActionPlan varchar(511),                                --The action plan provided by the auditor.
    FollowUpAudit INT REFERENCES Audits(ID),                --The audit in which this nonconformity is to be followed up in.
    isAddressed BIT NOT NULL,                               --Flag to signify whether the nonconformity has been addressed.
);

--*******************************END OF TABLE DEFINITIONS*********************************************





--**************************************CONSTRAINTS***************************************************



--**********************************END OF CONSTRAINTS************************************************


COMMIT TRANSACTION [CreateTbls]


END TRY
BEGIN CATCH
    SELECT 
        ERROR_NUMBER() AS ErrorNumber,
        ERROR_LINE() as ErrorLine,
        ERROR_MESSAGE() as ErrorMessage;

    ROLLBACK TRANSACTION [CreateTbls];
END CATCH 
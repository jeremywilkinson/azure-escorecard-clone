var isEdited=false;

function showOverlay(url, selected, data) {
    Url = url + "?selectedID="+selected;
    $(".modal-body").html("Loading please wait...");
    $("#InfoModal").modal("show");
    $(".modal-content").load(Url, data, function(){
        isEdited=false;
    });
    $(".modal-footer").html("<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>");
    
    $("#InfoModal").unbind();
}

function confirmClose(evt){
    if(isEdited){
        if(confirm("Are you sure you want to discard your changes?")){
            $("#InfoModal").modal("hide");
            isEdited=false;
        }
    } else {
        $("#InfoModal").modal("hide");
        isEdited=false;
    }
}

$(function(){
    $(document).on('change', 'input,textarea,select', function() {
        isEdited=true;
    });
    
    $(".modal-content").on('click', '.close,.closebtn,#InfoModal', confirmClose);
    $(document).on('click', '#InfoModal', confirmClose);
    
    $(".modal-content").click(function(evt) {
        evt.stopPropagation();
    });
});
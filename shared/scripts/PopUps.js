$(function(){
    $.ajaxSetup({xhr: function() {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function(evt) {
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    $(".progressBar").animate({width: percentComplete*100+"%"});
                }
            }, false);
            
            xhr.addEventListener("progress", function(evt){
            if (evt.lengthComputable) {
                var percentComplete = evt.loaded / evt.total;
                $(".progressBar").animate({width: (1-percentComplete)*100+"%"});
            }
            }, false);
            
            return xhr;
        }
    });
});
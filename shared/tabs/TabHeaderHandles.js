$(".tabheader").click(function () {
    if ($(this).data("add") != 1) {
        tabs = $(this).parent().children();
        for (var i = 0; i < tabs.length; i++) {
            if (tabs[i] != this) {
                $(tabs[i]).removeClass("selected");
            }
        }
        $(this).toggleClass("selected");
    }
});

$(".tabheader").click(function () {
    if ($(this).data("add") != 1) {
        children = $(this).parent().parent().parent().children();
        for (var i = 0; i < children.length; i++) {
            if ((children[i].id == this.id && $(this).hasClass("open") == false) || ($(children[i]).hasClass("open") && children[i].id != this.id)) {
                $(children[i]).animate({
                    opacity: 1,
                    left: "+=50",
                    height: "toggle"
                }, 500, function () {
                });
                $(children[i]).toggleClass("open");
            }
        }
    }
});
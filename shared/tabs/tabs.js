function removeEdit(tabheader){
    data=$(tabheader).html().split("<span style=\"right:-20px;\" class=\"glyphicon glyphicon-list\"></span>");
    
    $(tabheader).html("");
    for(i=0;i<data.length;i++){
        if(data[i]!="<span style=\"right:-20px;\" class=\"glyphicon glyphicon-list\"></span>") {
            $(tabheader).append(data[i]);
        }
    }
}

function addEdit(tabheader){
    if(tabheader.id!="positions" && tabheader.id!="employees"){
        $(tabheader).append("<span style=\"right:-20px;\" class=\"glyphicon glyphicon-list\"></span>");
    }
}

function toggleEdit(tabheader) {
    data=$(tabheader).html().split("<span style=\"right:-20px;\" class=\"glyphicon glyphicon-list\"></span>");
    
    if(data.length>1){
        removeEdit(tabheader);
    } else {
        addEdit(tabheader);
    }
}

function checkArrows(item){
    if(item.scrollLeft()!=0){
        item.siblings(".navArrowLeft").show();
    } else {
        item.siblings(".navArrowLeft").hide();
        item.siblings(".navArrowLeft").data("hover",0);
        item.siblings(".navArrowLeft").data("click",0);
    }
    
    if(item.scrollLeft()+item[0].clientWidth!=item[0].scrollWidth){
        item.siblings(".navArrowRight").show();
    } else {
        item.siblings(".navArrowRight").hide();
        item.siblings(".navArrowRight").data("hover",0);
        item.siblings(".navArrowRight").data("click",0);
    }
}

function Scroll(item, val){
    if($(item).data("hover")==1 || $(item).data("click")==1){
        $(item).siblings("ul").scrollLeft($(item).siblings("ul").scrollLeft()+val);
        setTimeout(function(){Scroll(item,val);},10);
        
        checkArrows($(item).siblings("ul"));
    }
}

function bindScrolls() {
    $(".headers").scroll(function(){
       checkArrows($(this)); 
    });
}

function checkAllArrows() {
    $(".headers").each(function(ind,item){
        checkArrows($(item));
    });
}

$(document).ready(function () {
    bindScrolls();
    
    checkAllArrows();
    
    $(document).on("mouseover", ".navArrowRight", function (){
        $(this).data("hover",1);
        Scroll($(this),1);
    });
    
    $(document).on("mouseout", ".navArrowRight", function (){
        $(this).data("hover",0);
    });
    
    $(document).on("mousedown", ".navArrowRight", function (){
        $(this).data("click",1);
        Scroll($(this),1);
    });
    
    $(document).on("mouseup", ".navArrowRight", function (){
        $(this).data("click",0);
    });
    
    $(document).on("mouseover", ".navArrowLeft", function (){
        $(this).data("hover",1);
        Scroll($(this),-1);
    });
    
    $(document).on("mouseout", ".navArrowLeft", function (){
        $(this).data("hover",0);
    });
    
    $(document).on("mousedown", ".navArrowLeft", function (){
        $(this).data("click",1);
        Scroll($(this),-1);
    });
    
    $(document).on("mouseup", ".navArrowLeft", function (){
        $(this).data("click",0);
    });
    
    $(document).on("click", ".tabheader", function (){
        
        if($(this).data("add") != 1){
            tabs = $(this).parent().children();
            for (var i = 0; i < tabs.length; i++) {
                if (tabs[i] != this) {
                    removeEdit(tabs[i]);
                }
            }
            toggleEdit(this);
        }
        
        if ($(this).data("add") != 1 && $(this).data("datatype")!="kpi") {
            tabs = $(this).parent().children();
            for (var i = 0; i < tabs.length; i++) {
                if (tabs[i] != this) {
                    $(tabs[i]).removeClass("selected");
                }
            }
            $(this).toggleClass("selected");
            
            children = $(this).parent().parent().parent().children();
            for (var i = 0; i < children.length; i++) {
                if ((children[i].id == this.id && $(this).hasClass("open") == false) || ($(children[i]).hasClass("open") && children[i].id != this.id)) {
                    $(children[i]).animate({
                        opacity: 1,
                        height: "toggle"
                    }, 500, function () {
                    });
                    $(children[i]).toggleClass("open");
                }
            }
        } else {
            eval($(this).data("onclick"));
        }
    });
    
    $(document).on("click", ".glyphicon-list", function (e){
        
        if($(this).parent()[0].id=="stores"){
            showOverlay("ajax/modify/modifyTemplates.cshtml");
        } else if ($(this).parent()[0].id=="audits"){
            showOverlay("ajax/modify/modifyAudits.cshtml");
        } else if ($(this).parent().data("datatype")=="pos"){
            showOverlay("ajax/view/viewPosition.cshtml", $(this).parent()[0].id);
        } else if ($(this).parent().data("datatype")=="emp"){
            showOverlay("ajax/view/viewEmployee.cshtml", $(this).parent()[0].id);
        } else if ($(this).parent().data("datatype")=="reg"){
            showOverlay("ajax/view/viewRegion.cshtml", $(this).parent()[0].id);
        } else if ($(this).parent().data("datatype")=="store"){
            showOverlay("ajax/view/viewStore.cshtml", $(this).parent()[0].id);
        } else if ($(this).parent().data("datatype")=="sa"){
            showOverlay("ajax/view/viewServiceArea.cshtml", $(this).parent()[0].id);
        } else if ($(this).parent().data("datatype")=="pkpa"){
            showOverlay("ajax/view/viewPKPA.cshtml", $(this).parent()[0].id);
        } else if ($(this).parent().data("datatype")=="ckpa"){
            showOverlay("ajax/view/viewCKPA.cshtml", $(this).parent()[0].id);
        } else if ($(this).parent().data("datatype")=="kpi"){
            showOverlay("ajax/view/viewKPI.cshtml", $(this).parent()[0].id);
        }
        
        e.stopPropagation();
    });

    $(".tabbody").animate({
        opacity: 1,
        height: "toggle"
    }, 0, function () {
    });

});
﻿/// <summary>
/// holds all the tabs
/// </summary>

using System.Collections.Generic;

public class TabsHeader
{
    List<Tab> tabs;
    string style;

    public TabsHeader(List<Tab> Tabs, string Style)
    {
        this.tabs=Tabs;
        this.style=Style;
    }

    public string GetHtml(){
        string text="";
        foreach(Tab tab in tabs){
            text+=tab.GetHtml();
        }
        return "<span class=\"navArrowLeft glyphicon glyphicon-chevron-left\"></span><ul style=\""+this.style+"\"class=\"headers\">"+text+"</ul><span class=\"navArrowRight glyphicon glyphicon-chevron-right\"></span>";
    }
}

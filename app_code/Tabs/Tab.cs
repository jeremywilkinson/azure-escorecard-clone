﻿/// <summary>
/// a single Tab
/// </summary>
public class Tab
{
    string datatype;
    string pairid;
    string displaycontent;
    string style;
    string onclick;

    public Tab(string Datatype, string Pairid, string Displaycontent, string Style, string onclick)
    {
        this.datatype = Datatype;
        this.pairid = Pairid;
        this.displaycontent = Displaycontent;
        this.style=Style;
        this.onclick=onclick;
    }

    public string GetHtml(){
        if(this.datatype=="add"){
            return "<li data-onclick=\""+this.onclick+"\" data-add=\"1\" style=\""+this.style+"\"id=\""+this.pairid+"\"class=\"tabheader add\">"+this.displaycontent+"</li>";
        } else {
            return "<li data-onclick=\""+this.onclick+"\" data-datatype=\""+this.datatype+"\" style=\""+this.style+"\"id=\""+this.pairid+"\"class=\"tabheader\">"+this.displaycontent+"</li>";
        }
        
    }
    
    public string GetPairID(){
        return this.pairid;
    }

}

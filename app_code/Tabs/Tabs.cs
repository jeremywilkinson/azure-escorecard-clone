﻿using System.Collections.Generic;

public class Tabs {

    List<TabBody> bodies;
    TabsHeader header;
    string style;
    string ID;

    public Tabs(List<Tab> tabs, List<TabBody> bodies, string headerstyle, string Style, string id){
        this.style=Style;

        this.header = new TabsHeader(tabs, headerstyle);
        this.bodies = bodies;

        this.ID = id;
    }

    public string GetHtml(){
        string bodiesstring="";
        foreach(TabBody body in this.bodies){
            bodiesstring+=body.GetHtml();
        }

        return "<div id=\""+this.ID+"\" style=\""+this.style+"\" class=\"tabs\"><div class=\"scrollhider\"><div class=\"lineextender\"></div>"+header.GetHtml()+"</div>"+bodiesstring+"</div>";
    }

}
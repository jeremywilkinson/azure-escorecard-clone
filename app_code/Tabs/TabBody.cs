﻿using System.Collections.Generic;

public class TabBody
{
    string pairid;
    string content;
    string style;

    public TabBody(string Pairid, string Content, string Style)
    {
        this.pairid = Pairid;
        this.content = Content;
        this.style = Style;
    }

    public string GetHtml(){
        return "<div style=\""+this.style+"\" id=\""+this.pairid+"\" class=\"tabbody\">"+this.content+"</div>";
    }
}

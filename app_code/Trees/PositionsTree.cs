﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

public class PositionsTree 
{
    Tabs tabs;

    string connetionString = null;
    SqlConnectionStringBuilder scsBuilder;
    SqlConnection cnn ;

    public Tabs recurseLevel(int level){
        List<TabBody> bodies = new List<TabBody>();
        List<Tab> tabheads = new List<Tab>();

        

        var sql = "SELECT * FROM Positions WHERE Super="+level; 

        try
        {

            SqlCommand command = new SqlCommand(sql, cnn);

            SqlDataReader reader = command.ExecuteReader();

            bool notAdmin=true;

            while (reader.Read()) {
                int ID=reader.GetInt32(0);
                string Title=reader.GetString(1);

                if(ID!=1){
                    Tab t = new Tab("pos", ""+ID ,Title, "", "");
                    tabheads.Add(t);
                } 
            }
            
            reader.Close();
            reader.Dispose();
            
            foreach(Tab t in tabheads){
                int ID = Convert.ToInt32(t.GetPairID());
                Tabs tempt = recurseLevel(ID);

                TabBody b = new TabBody(""+ID, tempt.GetHtml(), "");
                bodies.Add(b);
            }
            
            tabheads.Add(new Tab("add", "", "<span class=\"glyphicon glyphicon-plus\"></span>", "", "showOverlay('/ajax/newPosition.cshtml', $(this).parent().parent().parent().parent().attr('id'))"));
        } catch (SqlException ex){}

        return new Tabs(tabheads, bodies, "", "", "");
    }

    public PositionsTree(int start){
        Tabs Admin;
        
        List<TabBody> bodies = new List<TabBody>();
        List<Tab> tabheads = new List<Tab>();
        
        int connectionTimeoutSeconds = 30;  // Default of 15 seconds is too short over the Internet, sometimes.
        int maxCountTriesConnectAndQuery = 3;  // You can adjust the various retry count values.
        int secondsBetweenRetries = 4;  // Simple retry strategy.

        // [A.1] Prepare the connection string to Azure SQL Database.
        scsBuilder = new SqlConnectionStringBuilder();
        // Change these values to your values.
        scsBuilder["Server"] = "tcp:kznb9b607a.database.windows.net,1433";
        scsBuilder["User ID"] = "TastewiseAdmin";  // @yourservername suffix sometimes.
        scsBuilder["Password"] = "Tastewise1957";
        scsBuilder["Database"] = "eScorecard";
        // Leave these values as they are.
        scsBuilder["Trusted_Connection"] = false;
        scsBuilder["Integrated Security"] = false;
        scsBuilder["Encrypt"] = true;
        scsBuilder["Connection Timeout"] = connectionTimeoutSeconds;


		connetionString = scsBuilder.ToString();// "Server=tcp:kznb9b607a.database.windows.net,1433;Database=eScorecard;User ID=TastewiseAdmin@kznb9b607a;Password={Tastewise1957};Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
        cnn = new SqlConnection(connetionString);
        
        var sql = "SELECT * FROM Positions WHERE ID="+start;
        cnn.Open();
        
        SqlCommand command = new SqlCommand(sql, cnn);
        SqlDataReader reader = command.ExecuteReader();
        reader.Read();

        tabheads.Add(new Tab("pos", Convert.ToString(start), reader.GetString(1), "color: rgba(134, 123, 119,1)", ""));
        
        reader.Close();
        reader.Dispose();
        
        bodies.Add(new TabBody(Convert.ToString(start),recurseLevel(start).GetHtml(),""));

        Admin = new Tabs(tabheads, bodies, "", "", "");

        this.tabs = Admin;
        
        cnn.Close();
        cnn.Dispose();
    }

    public string GetHtml(){
        return this.tabs.GetHtml();
    }
}
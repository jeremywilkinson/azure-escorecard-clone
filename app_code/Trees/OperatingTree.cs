using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

public class OperatingTree 
{
    Tabs tabs;
    
    public Tabs recurseLevel(int level, string type){
        
        List<TabBody> bodies = new List<TabBody>();
        List<Tab> tabheads = new List<Tab>();
        
        string connetionString = null;
        SqlConnectionStringBuilder scsBuilder;
        SqlConnection cnn ;
        
        
        int connectionTimeoutSeconds = 30;  // Default of 15 seconds is too short over the Internet, sometimes.
        int maxCountTriesConnectAndQuery = 3;  // You can adjust the various retry count values.
        int secondsBetweenRetries = 4;  // Simple retry strategy.
        
        // [A.1] Prepare the connection string to Azure SQL Database.
        scsBuilder = new SqlConnectionStringBuilder();
        // Change these values to your values.
        scsBuilder["Server"] = "tcp:kznb9b607a.database.windows.net,1433";
        scsBuilder["User ID"] = "TastewiseAdmin";  // @yourservername suffix sometimes.
        scsBuilder["Password"] = "Tastewise1957";
        scsBuilder["Database"] = "eScorecard";
        // Leave these values as they are.
        scsBuilder["Trusted_Connection"] = false;
        scsBuilder["Integrated Security"] = false;
        scsBuilder["Encrypt"] = true;
        scsBuilder["Connection Timeout"] = connectionTimeoutSeconds;
        
        connetionString = scsBuilder.ToString();// "Server=tcp:kznb9b607a.database.windows.net,1433;Database=eScorecard;User ID=TastewiseAdmin@kznb9b607a;Password={Tastewise1957};Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
        cnn = new SqlConnection(connetionString);
        
        string nType = "";
        string sql = "SELECT * FROM Regions WHERE Name=''";
        string fulltype = "";
        
        if(type == "nul"){
            sql = "SELECT * FROM Regions"; 
            nType = "reg";
            fulltype = "Region";
        } else if(type == "reg"){
            sql = "SELECT * FROM Stores WHERE Region="+level; 
            nType = "store";
            fulltype = "Store";
        } else if(type=="store"){
            sql = "SELECT * FROM ServiceAreas LEFT JOIN ServiceAreaTemplates ON ServiceAreas.Type=ServiceAreaTemplates.ID WHERE Parent="+level; 
            nType = "sa";
            fulltype = "ServiceArea";
        } else if(type=="sa"){
            sql = "SELECT * FROM KPAs LEFT JOIN KPATemplates ON KPAs.Type=KPATemplates.ID WHERE isParent=1 AND Parent="+level; 
            nType = "pkpa";
            fulltype = "PKPA";
        } else if(type=="pkpa"){
            sql = "SELECT * FROM KPAs LEFT JOIN KPATemplates ON KPAs.Type=KPATemplates.ID WHERE isParent=0 AND Parent="+level; 
            nType = "ckpa";
            fulltype = "CKPA";
        } else if(type=="ckpa"){
            sql = "SELECT * FROM KPIs LEFT JOIN KPITemplates ON KPIs.Type=KPITemplates.ID WHERE Parent="+level; 
            nType = "kpi";
            fulltype = "KPI";
        }
        try
        {
            cnn.Open();

            SqlCommand command = new SqlCommand(sql, cnn);

            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read()) {
                int ID=reader.GetInt32(0);
                string Title="";
                
                if(type=="store" || type=="sa" || type=="pkpa" || type=="ckpa"){
                    Title=reader.GetString(5);
                } else {
                    Title=reader.GetString(1);
                }

                Tab t = new Tab(nType, ""+ID ,Title, "", "");
                tabheads.Add(t);

                Tabs tempt = recurseLevel(ID, nType);

                TabBody b = new TabBody(""+ID, tempt.GetHtml(), "");
                bodies.Add(b);
            }

            if(type!="kpi"){
                tabheads.Add(new Tab("add", "", "<span class=\"glyphicon glyphicon-plus\"></span>", "", "showOverlay('/ajax/new"+fulltype+".cshtml', $(this).parent().parent().parent().parent().attr('id'))"));
            }
            
            cnn.Close();
            cnn.Dispose();

            reader.Close();
            reader.Dispose();
        } catch (SqlException ex){}
        
        return new Tabs(tabheads, bodies, "", "", "");
    }

    public OperatingTree(){
        this.tabs = recurseLevel(0,"nul");
    }

    public string GetHtml(){
        return this.tabs.GetHtml();
    }
}
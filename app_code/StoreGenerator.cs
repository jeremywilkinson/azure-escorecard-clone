using System;
using System.Data.SqlClient;
using System.Collections.Generic;

public class StoreGenerator : Item {

    public StoreGenerator() : base(){
    }

    public StoreC[] genAll(){
        List<StoreC> Stores = new List<StoreC>();
        
        sql="SELECT ID FROM Stores";
        SqlDataReader data = execQuery(sql);
        
        while (data.Read()){
            Stores.Add(new StoreC((int) data["ID"]));
        }
        
        return Stores.ToArray();
    }

}
using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;

public class CompletedAuditFieldGenerator : Item {

    public CompletedAuditFieldGenerator() : base(){
    }

    public CompletedAuditFieldC[] genAll(){
        List<CompletedAuditFieldC> CompletedAuditFields = new List<CompletedAuditFieldC>();
        
        sql="SELECT * FROM CompletedAuditFields";
        SqlDataReader data = execQuery(sql);
        
        while (data.Read()){
            CompletedAuditFields.Add(new CompletedAuditFieldC(data));
        }
        
        return CompletedAuditFields.ToArray();
    }
    
    public CompletedAuditFieldC[] genByAudit(int Audit){
        List<CompletedAuditFieldC> CompletedAuditFields = new List<CompletedAuditFieldC>();
        
        sql="SELECT * FROM CompletedAuditFields WHERE AuditID="+Audit;
        SqlDataReader data = execQuery(sql);
        
        while (data.Read()){
            CompletedAuditFields.Add(new CompletedAuditFieldC(data));
        }
        
        return CompletedAuditFields.ToArray();
    }

}
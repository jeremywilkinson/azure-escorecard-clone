using System.Data.SqlClient;

public class ServiceAreaC : Item {

    int ID;
    int Type;
    int Parent;
    decimal Weighting;
    SATemplateC SATemplate;

    public ServiceAreaC(int ID) : base(){
        load(ID);
    }
    
    public int load(int ID){
        sql="SELECT * FROM ServiceAreas WHERE ID="+ID;
        SqlDataReader data=execQuery(sql);
        data.Read();
        this.ID=ID;
        this.Parent=(int) data["Parent"];
        this.Weighting=(decimal) data["Weighting"];
        this.Type=(int) data["Type"];
        loadTemplate(Type);
        
        this.closeCNN();
        
        return 0;
    }
    
    public int loadTemplate(int ID){
        SATemplate = new SATemplateC(ID);
        
        return 0;
    }
    
    public int getID(){
        return ID;
    }
    
    public StoreC getParentStore(){
        return new StoreC(this.Parent);
    }
    
    public int getParentID(){
        return Parent;
    }
    
    public SATemplateC getTemplate(){
        return SATemplate;
    }
    
    public int getType(){
        return this.Type;
    }
    
    public string getName(){
        return SATemplate.getName();
    }

    public decimal getWeighting(){
        return this.Weighting*100;
    }
    
    public string Print(decimal Score){
        return "<tr><td class=\"left SA\">Service Area: "+this.getName()+"</td><td class=acceptableRange></td><td class=\"right\">"+Score+"%</td></tr>";
    }
}
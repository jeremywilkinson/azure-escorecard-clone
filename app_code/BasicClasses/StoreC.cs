using System.Data.SqlClient;

public class StoreC : Item {

    int ID;
    string Name;
    int Super;
    int Region;
    string Address;
    string ContactNumber;
    string Email;
    string StoreNumber;

    public StoreC(int ID) : base(){
        load(ID);
    }
    
    public int load(int ID){
        sql="SELECT * FROM Stores WHERE ID="+ID;
        SqlDataReader data=execQuery(sql);
        data.Read();
        this.ID=ID;
        this.Name=data["Name"] as string;
        this.Super=(int) data["Super"];
        this.Region=(int) data["Region"];
        this.Address=data["Address"] as string;
        this.ContactNumber=data["ContactNumber"] as string;
        this.Email=data["Email"] as string;
        this.StoreNumber=data["StoreNumber"] as string;
        
        this.closeCNN();
        
        return 0;
    }
    
    public int saveChanges(){
        sql="UPDATE Stores SET Name='"+this.Name+"', Super="+this.Super+", Region="+this.Region+", Address='"+this.Address+"', ContactNumber='"+this.ContactNumber+"', Email='"+this.Email+"', StoreNumber='"+this.StoreNumber+"' WHERE ID="+this.ID;
        execQuery(sql);
        return 0;
    }
    
    public int getID() {
        return ID;
    }
    
    public string getName() {
        return Name;
    }
    
    public EmployeeC getSuper(){
        return new EmployeeC(this.Super);
    }
    
    public RegionC getRegion(){
        return new RegionC(this.Region);
    }
    
    public string getAddress(){
        return this.Address;
    }
    
    public string getContactNumber(){
        return this.ContactNumber;
    }
    
    public string getEmail(){
        return this.Email;
    }
    
    public string getStoreNumber(){
        return this.StoreNumber;
    }

    public int setName(string Name){
        if (Name.Length<255){
            this.Name=Name;
            return 0;
        }
        return 1;
    }
    
    public int setAddress(string Address){
        if (Address.Length<500){
            this.Address=Address;
            return 0;
        }
        return 1;
    }
    
    public int setContactNumber(string ContactNumber){
        if (ContactNumber.Length==10 || ContactNumber.Length==0){
            this.ContactNumber=ContactNumber;
            return 0;
        }
        return 1;
    }
    
    public int setSuper(int Super){
        EmployeeGenerator gen = new EmployeeGenerator();
        
        if (gen.Exists(Super)){
            this.Super=Super;
            return 0;
        }
        return 1;
    }
    
    public int setRegion(int Region){
        RegionGenerator gen = new RegionGenerator();
        
        if (gen.Exists(Region)){
            this.Region=Region;
            return 0;
        }
        return 1;
    }
    
    public int setStoreNumber(string StoreNumber){
        if (StoreNumber.Length<10){
            this.StoreNumber=StoreNumber;
            return 0;
        }
        return 1;
    }
    
    public int setEmail(string Email){
        if (Email.Length<50){
            this.Email=Email;
            return 0;
        }
        return 1;
    }
}
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;

public class EmployeeC : Item {

    int ID;
    PositionC Position;
    int Super;
    string Username;
    string FirstName;
    string LastName;
    string Password;
    string ContactNumber;
    string Image;
    string EmployeeNumber;
    string IDNumber;
    string Email;
    int? Store;

    public EmployeeC(int ID) : base(){
        load(ID);
    }
    
    public EmployeeC(SqlDataReader data) : base(){
        load(data);
    }
    
    public EmployeeC(string username) : base(){
        load(username);
    }
    
    public int load(int ID){
        sql="SELECT * FROM Employees WHERE ID="+ID;
        SqlDataReader data=execQuery(sql);
        data.Read();
        this.ID=ID;
        this.Super=(int) data["Super"];
        this.FirstName=data["FirstName"] as string;
        this.LastName=data["LastName"] as string;
        this.Username=data["Username"] as string;
        this.Password=data["Password"] as string;
        this.ContactNumber=data["ContactNumber"] as string;
        this.Image=data["Image"] as string;
        this.EmployeeNumber=data["EmployeeNumber"] as string;
        this.IDNumber=data["IDNumber"] as string;
        this.Email=data["Email"] as string;
        this.Store=data["Store"] as int?;
        loadPosition((int) data["Position"]);
        
        this.closeCNN();
        
        return 0;
    }
    
    public int load(string username){
        sql="SELECT * FROM Employees WHERE Username='"+username+"'";
        SqlDataReader data=execQuery(sql);
        data.Read();
        this.ID=(int) data["ID"];
        this.Super=(int) data["Super"];
        this.FirstName=data["FirstName"] as string;
        this.LastName=data["LastName"] as string;
        this.Username=data["Username"] as string;
        this.Password=data["Password"] as string;
        this.ContactNumber=data["ContactNumber"] as string;
        this.Image=data["Image"] as string;
        this.EmployeeNumber=data["EmployeeNumber"] as string;
        this.IDNumber=data["IDNumber"] as string;
        this.Email=data["Email"] as string;
        this.Store=data["Store"] as int?;
        loadPosition((int) data["Position"]);
        
        this.closeCNN();
        
        return 0;
    }
    
    public int load(SqlDataReader data){
        this.ID=(int) data["ID"];
        this.Super=(int) data["Super"];
        this.FirstName=data["FirstName"] as string;
        this.LastName=data["LastName"] as string;
        this.Username=data["Username"] as string;
        this.Password=data["Password"] as string;
        this.ContactNumber=data["ContactNumber"] as string;
        this.Image=data["Image"] as string;
        this.EmployeeNumber=data["EmployeeNumber"] as string;
        this.IDNumber=data["IDNumber"] as string;
        this.Email=data["Email"] as string;
        this.Store=data["Store"] as int?;
        loadPosition((int) data["Position"]);
        
        return 0;
    }
    
    public int saveChanges() {
        string Store;
        if(this.Store==null){
            Store="NULL";
        } else {
            Store=this.Store.ToString();
        }
        
        sql="UPDATE Employees SET FirstName='"+this.FirstName+"',LastName='"+this.LastName+"',Username='"+this.Username+"',Password='"+this.Password+"',ContactNumber='"+this.ContactNumber+"',EmployeeNumber='"+this.EmployeeNumber+"',IDNumber='"+this.IDNumber+"',Email='"+this.Email+"',Store="+Store+" WHERE ID="+this.ID;
        
        SqlDataReader data=execQuery(sql);
        
        this.closeCNN();
        
        return 0;
    }
    
    public int loadPosition(int ID){
        Position = new PositionC(ID);
        
        return 0;
    }
    
    public int getID(){
        return this.ID;
    }
    
    public string getEmployeeNumber(){
        return this.EmployeeNumber;
    }
    
    public int? getStoreID() {
        return this.Store;
    }
    
    public EmployeeC getSupervisor() {
        return new EmployeeC(this.Super);
    }
    
    public string getFirstName(){
        return FirstName;
    }
    
    public string getLastName(){
        return LastName;
    }
    
    public string getFullName(){
        return getFirstName()+" "+getLastName();
    }
    
    public string getIDNumber(){
        return IDNumber;
    }
    
    public string getIDNumberHtml(){
        if(getIDNumber()!=""){
            return "<span>"+getIDNumber()+"</span>";
        } else {
            return "<span class=NoDesc>None</span>";
        }
    }
    
    public string getUsername(){
        return Username;
    }
    
    public string getContactNumber(){
        return ContactNumber;
    }
    
    public PositionC getPosition(){
        return Position;
    }
    
    public string getEmail(){
        return Email;
    }

    public int getAccountType() {
        return getPosition().getAccountType();
    }
    
    public StoreC getStore(){
        if(this.Store==null){
            return null;
        }
        return new StoreC(this.Store ?? default(int));
    }
    
    public string getStoreName(){
        return getStore().getName();
    }
    
    public string getStoreHtml(){
        if(this.Store==null){
            return "<span class=NoDesc>None</span>";
        } else {
            return "<span>"+getStoreName()+"</span>";
        }
    }
    
    public int setFirstName(string FirstName){
        if(FirstName.Length<=255){
            this.FirstName=FirstName;
            return 0;
        }
        return 1;
    }
    
    public int setLastName(string LastName){
        if(LastName.Length<=255){
            this.LastName=LastName;
            return 0;
        }
        return 1;
    }
    
    public int setUsername(string Username){
        if(Username.Length<=255){
            this.Username=Username;
            return 0;
        }
        return 1;
    }
    
    public int setPassword(string Password){
        SHA1 sha1 = SHA1.Create();
        byte[] hashData = sha1.ComputeHash(Encoding.Default.GetBytes(Password));
        StringBuilder returnValue = new StringBuilder();
        for (int i = 0; i < hashData.Length; i++)
        {
            returnValue.AppendFormat("{0:x2}", hashData[i]);
        }
        
		this.Password = returnValue.ToString();
        
        return 0;
    }
    
    public int setStore(int? Store){
        this.Store=Store;
        return 0;
    }

    public int setContactNumber(string ContactNumber){
        if(ContactNumber.Length==10 || ContactNumber.Length==0){
            this.ContactNumber=ContactNumber;
            return 0;
        }
        return 1;
    }
    
    public int setIDNumber(string IDNumber){
        if(IDNumber.Length==13 || IDNumber.Length==0){
            this.IDNumber=IDNumber;
            return 0;
        }
        return 1;
    }
    
    public int setEmail(string Email){
        if(Email.Length<=50){
            this.Email=Email;
            return 0;
        }
        return 1;
    }
    
    public int setEmployeeNumber(string EmployeeNumber){
        if(EmployeeNumber.Length<10){
            this.EmployeeNumber=EmployeeNumber;
            return 0;
        }
        return 1;
    }
    
}
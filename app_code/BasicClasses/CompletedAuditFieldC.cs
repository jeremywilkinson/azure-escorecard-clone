using System.Data.SqlClient;
using System;

public class CompletedAuditFieldC : Item {

    int ID;
    int AuditID;
    int KPI;
    string Comment;
    decimal Input;

    public CompletedAuditFieldC(int ID) : base(){
        load(ID);
    }
    
    public CompletedAuditFieldC(SqlDataReader data) : base(){
        load(data);
    }
    
    public int load(int ID){
        sql="SELECT * FROM CompletedAuditFields WHERE ID="+ID;
        SqlDataReader data=execQuery(sql);
        data.Read();
        this.ID=ID;
        this.AuditID=(int) data["AuditID"];
        this.KPI=(int) data["KPI"];
        this.Comment=data["Comment"] as string;
        this.Input=(decimal) data["Input"];
        
        this.closeCNN();
        
        return 0;
    }
    
    public int load(SqlDataReader data){
        this.ID=(int) data["ID"];
        this.AuditID=(int) data["AuditID"];
        this.KPI=(int) data["KPI"];
        this.Comment=data["Comment"] as string;
        this.Input=(decimal) data["Input"];
        
        return 0;
    }
    
    public int getID() {
        return ID;
    }
    
    public AuditC getAudit(){
        return  new AuditC(AuditID);
    }
    
    public string getComment(){
        return Comment;
    }
    
    public NonconformityC getNonconformity(){
        if(isComplient()){
            return null;
        }
        
        sql="SELECT * FROM Nonconformities WHERE CompletedAuditField="+getID();
        SqlDataReader data=execQuery(sql);
        data.Read();
        return new NonconformityC((int) data["ID"]);
    }
    
    public string getCommentHtml(){
        string comment=getComment();
        if (comment=="") {
            return "<span class=noDesc>None</span>";
        } else {
            return "<span>"+comment+"</span>";
        }
    }
    
    public int getType() {
        return getKPI().getType();
    }

    public KPIC getKPI() {
        return new KPIC(this.KPI);
    }
    
    public decimal getInput(){
        return Input;
    }
    
    public int getInputType() {
        return this.getKPI().getInputType();
    }
    
    public bool isComplient(){
        return this.getKPI().getTemplate().isComplient(this.getInput());
    }
    
    public string getInputString(){
        if(getInputType()==1){
            return this.Normalize(this.getInput())+" "+this.getKPI().getTemplate().getUnits();
        } else if (getInputType()==2){
            return Convert.ToBoolean(this.getInput()).ToString();
        } else if (getInputType()==3){
            return "Rating "+Decimal.ToInt32(this.getInput()).ToString();
        } else if(getInputType()==4){
            return this.Normalize(this.getInput())+" seconds";
        }
        
        return "";
    }
    
    public string getInputDisplay(){
        if(getInputType()==1){
            return "Original value: "+getInputString();
        } else if (getInputType()==2){
            return "Original state: "+getInputString();
        } else if (getInputType()==3){
            return "Original rating: "+getInputString();
        } else if(getInputType()==4){
            return "Original duration: "+getInputString();
        }
        
        return "";
    }
    
    public string getInputHtml(){
        if(getInputType()==1){
            return getInputString();
        } else if (getInputType()==2){
            return getInputString();
        } else if (getInputType()==3){
            string html="<span>";
            
            for (int i = 1; i < 6; i++) {
                if(i==Decimal.ToInt32(this.getInput())){
                    html+="<input type=radio selected> "+i;
                } else {
                    html+="<input type=radio> "+i;
                }
            }
            
            return html+"</span>";
        } else {
            return getInputString();
        }
    }
    
    public string getColoredInputString(){
        if(this.isComplient()){
            return "<span style='color: green'>"+getInputString()+"</span>";
        } else {
            return "<span style='color: red'>"+getInputString()+"</span>";
        }
    }
    
    public string getColoredInputHtml(){
        if(this.isComplient()){
            return "<span style='color: green'>"+getInputHtml()+"</span>";
        } else {
            return "<span style='color: red'>"+getInputHtml()+"</span>";
        }
    }
    
    public EmployeeC[] getResponsiblePeople(){
        return this.getKPI().getResponsiblePeople();
    }
    
    public string getEmailTableEntry(){
        return @"<tr>
                    <td style='
                        border-top: 1px solid black;
                        text-align: left;
                        padding-left: 5px;'>"+this.getKPI().getName()+@"</td>
                        <td style='
                        width: 7ch;
                        padding-left: 5px;
                        padding-right: 9px;
                        border-left: 1px solid black;
                        border-top: 1px solid black;'>"+this.getColoredInputString()+@"</td>
                </tr>";
    }
    
    public string Print(){
        KPIC KPI = this.getKPI();
        string html = "<tr><td class=\"left KPI\">KPI: "+KPI.getName();
        
        if(getComment()!=""){
            html+="<span class=\"commentContainer\"><img src=\"http://tastewisescorecard.azurewebsites.net/media/c.png\" class=\"comment\">: <span class=\"commentWrapper\"\">\""+getComment()+"\"</span></span>";
        }
        
        if(!isComplient()){
            html+=getNonconformity().Print();
        }
        
        html+="</td><td class=\"acceptableRange\">"+this.getKPI().getAcceptableRangeString()+"</td>";
        
        html+="<td class=\"right\">"+this.getColoredInputString();
        
        if(isComplient()){
            html+="<span class=\"glyphicon glyphicon-ok Complient\"></span>";
        } else {
            html+="<span class=\"glyphicon glyphicon-remove nonComplient\"></span>";
            
        }
        
        html+="</td></tr>";
        
        return html;
    }

}
using System.Data.SqlClient;

public class KPIC : Item {

    int ID;
    int Type;
    int Parent;
    decimal Weighting;
    KPITemplateC KPITemplate;

    public KPIC(int ID) : base(){
        load(ID);
    }
    
    public int load(int ID){
        sql="SELECT * FROM KPIs WHERE ID="+ID;
        SqlDataReader data=execQuery(sql);
        data.Read();
        this.ID=ID;
        this.Parent=(int) data["Parent"];
        this.Weighting=(decimal) data["Weighting"];
        this.Type=(int) data["Type"];
        loadTemplate(Type);
        
        this.closeCNN();
        
        return 0;
    }
    
    public int loadTemplate(int ID){
        KPITemplate = new KPITemplateC(ID);
        
        return 0;
    }
    
    public string getAcceptableRangeString(){
        return this.KPITemplate.getAcceptableRangeString();
    }
    
    public int getID(){
        return this.ID;
    }
    
    public int getType(){
        return this.Type;
    }
    
    public int getInputType(){
        return this.getTemplate().getType();
    }
    
    public string getName(){
        return this.getTemplate().getName();
    }
    
    public decimal getWeighting(){
        return this.Weighting*100;
    }
    
    public KPITemplateC getTemplate(){
        return KPITemplate;
    }
    
    public CKPAC getParentCKPA(){
        return new CKPAC(this.Parent);
    }
    
    public PKPAC getParentPKPA(){
        return this.getParentCKPA().getParentPKPA();
    }
    
    public ServiceAreaC getParentSA(){
        return this.getParentPKPA().getParentSA();
    }
    
    public StoreC getParentStore(){
        return this.getParentSA().getParentStore();
    }
    
    public EmployeeC[] getResponsiblePeople(){
        EmployeeGenerator gen = new EmployeeGenerator();
        
        return gen.genByPositionsAndStore(this.KPITemplate.getResponsiblePositions(),this.getParentStore());
    }
    
    public string Print(decimal Score){
        return "<tr><td class=\"left KPI\">KPI: "+this.getName()+"</td><td></td class=acceptableRange><td class=\"right\">"+Score+"</td></tr>";
    }

}
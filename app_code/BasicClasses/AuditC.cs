using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

public class AuditC : Item {

    int ID;
    DateTime DateScheduled;
    AuditTemplateC AuditTemplate;
    int Super;
    int Store;
    bool Completed;
    DateTime Completion;
    CompletedAuditFieldC[] CompFields;
    KPIC[] KPIs;

    public AuditC(int ID) : base(){
        load(ID);
    }
    
    public int load(int ID){
        sql="SELECT * FROM Audits WHERE ID="+ID;
        SqlDataReader data=execQuery(sql);
        data.Read();
        this.ID=ID;
        this.DateScheduled=(DateTime) data["DateScheduled"];
        this.Super=(int) data["Super"];
        this.Store=(int) data["Store"];
        this.Completed=(bool) data["Completed"];
        this.Completion=(DateTime) data["Completion"];
        loadTemplate((int) data["Type"]);
        
        this.closeCNN();
        
        return 0;
    }
    
    public int getStoreID() {
        return Store;
    }
    
    public StoreC getStore() {
        return new StoreC(Store);
    }
    
    void loadTemplate(int ID){
        this.AuditTemplate=new AuditTemplateC(ID);
    }
    
    public AuditTemplateC getAuditTemplate(){
        return AuditTemplate;
    }
    
    public int getID() {
        return ID;
    }
    
    public string getName(){
        return AuditTemplate.getName();
    }
    
    public DateTime getCompletion(){
        return Completion;
    }
    
    public DateTime getDateScheduled(){
        return DateScheduled;
    }
    
    public string getCompletionString(){
        return this.formatDateTime(getCompletion());
    }
    
    public string getDateScheduledString(){
        return this.formatDateTime(getDateScheduled());
    }
    
    public EmployeeC getSuper(){
        return new EmployeeC(this.Super);
    }
    
    public CompletedAuditFieldC[] getCompletedAuditFields(CKPAC CKPA){
        if(CompFields==null){
            CompletedAuditFieldGenerator gen = new CompletedAuditFieldGenerator();
            this.CompFields=gen.genByAudit(this.getID());
        }
        if(CKPA==null){
            return this.CompFields;
        } else {
            List<CompletedAuditFieldC> Comps = new List<CompletedAuditFieldC>();
            
            foreach(CompletedAuditFieldC CompField in getCompletedAuditFields(null)){
                if(CompField.getKPI().getParentCKPA().getID()==CKPA.getID()){
                    Comps.Add(CompField);
                }
            }
            return Comps.ToArray();
        }
    }
    
    public void loadKPIs(){
        sql="SELECT * FROM CompletedAuditFields WHERE AuditID="+this.getID();
        List<KPIC> KPIs = new List<KPIC>();
        
        var results=execQuery(sql);
        
        while (results.Read()) {
            KPIs.Add(new KPIC((int) results["KPI"]));
        }
        this.KPIs=KPIs.ToArray();
    }
    
    public KPIC[] getKPIs(CKPAC CKPA){
        if(this.KPIs==null){
            loadKPIs();
        }
        
        if(CKPA==null){
            return this.KPIs;
        } else {
            List<KPIC> KPIs = new List<KPIC>();
            
            foreach(KPIC KPI in this.KPIs){
                if (KPI.getParentCKPA().getID()==CKPA.getID()){
                    KPIs.Add(KPI);
                }
            }
            return KPIs.ToArray();
        }
    }
    
    public ServiceAreaC[] getServiceAreas(){
        KPIC[] KPIs = getKPIs(null);
        List<ServiceAreaC> SAs = new List<ServiceAreaC>();
        
        foreach(KPIC KPI in KPIs){
            var pSA=KPI.getParentSA();
            if(SAs.FindIndex(SA => SA.getID() == pSA.getID())==-1){
                SAs.Add(pSA);
            }
        }
        
        return SAs.ToArray();
    }
    
    public PKPAC[] getPKPAs(ServiceAreaC SA){
        KPIC[] KPIs = getKPIs(null);
        List<PKPAC> PKPAs = new List<PKPAC>();
        
        foreach(KPIC KPI in KPIs){
            if(SA==null){
                PKPAs.Add(KPI.getParentPKPA());
            } else if (KPI.getParentSA().getID()==SA.getID()){
                var pPKPA = KPI.getParentPKPA();
                if(PKPAs.FindIndex(PKPA => PKPA.getID() == pPKPA.getID())==-1){
                    PKPAs.Add(pPKPA);
                }
            }
        }
        
        return PKPAs.ToArray();
    }
    
    public CKPAC[] getCKPAs(PKPAC PKPA){
        KPIC[] KPIs = getKPIs(null);
        List<CKPAC> CKPAs = new List<CKPAC>();
        
        foreach(KPIC KPI in KPIs){
            if(PKPA==null){
                CKPAs.Add(KPI.getParentCKPA());
            } else if(KPI.getParentPKPA().getID()==PKPA.getID()){
                var pCKPA = KPI.getParentCKPA();
                if(CKPAs.FindIndex(CKPA => CKPA.getID() == pCKPA.getID())==-1){
                    CKPAs.Add(pCKPA);
                }
            }
        }
        
        return CKPAs.ToArray();
    }
    
    public decimal getKPIScore(KPIC KPI){
        foreach(CompletedAuditFieldC comp in this.getCompletedAuditFields(null)){
            if (comp.getKPI().getID()==KPI.getID()){
                if(comp.isComplient()){
                    return 100.00m;
                } else {
                    return 000.00m;
                }
            }
        }
        return 000.00m;
    }
    
    public decimal getCKPAScore(CKPAC CKPA){
        KPIC[] KPIs = this.getKPIs(CKPA);
        
        if(KPIs.Length==0){
            return 0m;
        }
        
        var totalWeighting=0m;
        var totalScore=0m;
        foreach(KPIC KPI in KPIs){
            totalWeighting+=KPI.getWeighting();
            totalScore+=this.getKPIScore(KPI)*KPI.getWeighting();
        }
        return totalScore/totalWeighting;
    }
    
    public decimal getPKPAScore(PKPAC PKPA){
        CKPAC[] CKPAs = this.getCKPAs(PKPA);
        
        var totalWeighting=0m;
        var totalScore=0m;
        foreach(CKPAC CKPA in CKPAs){
            totalWeighting+=CKPA.getWeighting();
            totalScore+=this.getCKPAScore(CKPA)*CKPA.getWeighting();
        }
        return totalScore/totalWeighting;
    }
    
    public decimal getSAScore(ServiceAreaC SA){
        PKPAC[] PKPAs = this.getPKPAs(SA);
        
        var totalWeighting=0m;
        var totalScore=0m;
        foreach(PKPAC PKPA in PKPAs){
            totalWeighting+=PKPA.getWeighting();
            totalScore+=this.getPKPAScore(PKPA)*PKPA.getWeighting();
        }
        return totalScore/totalWeighting;
    }
    
    public decimal getTotalScore(){
        ServiceAreaC[] SAs = this.getServiceAreas();
        
        var totalWeighting=0m;
        var totalScore=0m;
        foreach(ServiceAreaC SA in SAs){
            totalWeighting+=SA.getWeighting();
            totalScore+=this.getSAScore(SA)*SA.getWeighting();
        }
        return totalScore/totalWeighting;
    }
    
    public string sendFeedbackEmails() {
        if(Completed){
            string Recipients="";
            
            Dictionary<int, List<int>> EmployeeResponsibilities = new Dictionary<int, List<int>>();
            CompletedAuditFieldC[] Comps=this.getCompletedAuditFields(null);
            
            foreach (CompletedAuditFieldC comp in Comps)
            {
                EmployeeC[] Employees = comp.getResponsiblePeople();
                foreach(EmployeeC employee in Employees){
                    if(!EmployeeResponsibilities.ContainsKey(employee.getID())){
                        EmployeeResponsibilities.Add(employee.getID(),new List<int>());
                        Recipients+="Sent feedback eMail to: "+employee.getFullName()+"("+employee.getEmail()+")<br>";
                    }
                    EmployeeResponsibilities[employee.getID()].Add(comp.getID());
                }
            }
            
            EmailSender sender = new EmailSender();
            
            string eMailTemplate=File.ReadAllText("D:\\home\\site\\wwwroot\\EmailTemplates\\PersonalFeedback.html");
        
            foreach(int emp in EmployeeResponsibilities.Keys){
                string eMail=eMailTemplate;
                EmployeeC employee = new EmployeeC(emp);
                
                eMail=eMail.Replace("@Name",employee.getFullName());
                eMail=eMail.Replace("@Type",this.getName());
                eMail=eMail.Replace("@Date",this.getCompletionString());
                eMail=eMail.Replace("@Store",this.getStore().getName());
                
                string kpihtml="";
                
                int numPassed=0;
                
                foreach(int compfield in EmployeeResponsibilities[emp]){
                    CompletedAuditFieldC completed = new CompletedAuditFieldC(compfield);
                    kpihtml+=completed.getEmailTableEntry();
                    
                    if(completed.isComplient()){
                        numPassed+=1;
                    }
                }
                
                eMail=eMail.Replace("@KPIHTML",kpihtml);
                eMail=eMail.Replace("@KPITotal",(100*numPassed/EmployeeResponsibilities[emp].Count).ToString());
                sender.send(eMail,"eScorecard Personal Feedback "+this.getName()+" "+this.getCompletionString(),employee.getEmail());
            }
            
            return Recipients;
        } else {
            return null;
        }
    }
    
    public string getPrintHeader(){
        string html="<span class=heading>"+this.getName()+" Audit - "+this.getStore().getName()+"</span><br>";
        html+="<span>Scheduled for: "+this.getDateScheduledString()+"<br>Completed by: "+this.getCompletionString()+"<br>";
        html+="<br><b>Total Score: </b>"+this.getTotalScore()+"</span>";
        
        return html;
    }

}
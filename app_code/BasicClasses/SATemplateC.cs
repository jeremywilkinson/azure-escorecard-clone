using System;
using System.Data.SqlClient;

public class SATemplateC : Item {

    int ID;
    string Name;
    string Policy;

    public SATemplateC(int ID) : base(){
        load(ID);
    }

    public int load(int ID){
        sql="SELECT * FROM ServiceAreaTemplates WHERE ID="+ID;
        SqlDataReader data=execQuery(sql);
        data.Read();
        this.ID=ID;
        this.Name=(string) data["Name"];
        this.Policy=(string) data["Policy"];
        
        this.closeCNN();
        
        return 0;
    }
    
    public int saveChanges(){
        sql="UPDATE ServiceAreaTemplates SET Name='"+this.Name+"', Policy='"+this.Policy+"' WHERE ID="+this.ID;
        execQuery(sql);
        return 0;
    }
    
    public int getID(){
        return this.ID;
    }
    
    public string getName(){
        return this.Name;
    }
    
    public string getPolicy(){
        return this.Policy;
    }
    
    public string getPolicyHtml(){
        if(this.getPolicy()==""){
            return "<span class=NoDesc>None</span>";
        } else {
            return "<span>"+getPolicy()+"</span>";
        }
    }
    
    public int setName(string Name){
        if (Name.Length<255){
            this.Name=Name;
            return 0;
        }
        return 1;
    }

    public int setPolicy(string Policy){
        if (Policy.Length<2000){
            this.Policy=Policy;
            return 0;
        }
        return 1;
    }

}
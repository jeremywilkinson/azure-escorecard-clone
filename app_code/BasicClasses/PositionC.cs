using System.Data.SqlClient;

public class PositionC : Item {

    int ID;
    string Title;
    int Super;
    string Description;
    int AccountType;

    public PositionC(int ID) : base(){
        load(ID);
    }
    
    public PositionC(SqlDataReader data) : base(){
        load(data);
    }
    
    public int load(int ID){
        sql="SELECT * FROM Positions WHERE ID="+ID;
        SqlDataReader data=execQuery(sql);
        data.Read();
        this.ID=ID;
        this.Title=data["Title"] as string;
        this.Super=(int) data["Super"];
        this.Description=data["Description"] as string;
        this.AccountType=(int) data["AccountType"];
        
        this.closeCNN();
        
        return 0;
    }
    
    public int load(SqlDataReader data){
        this.ID=(int) data["ID"];
        this.Title=data["Title"] as string;
        this.Super=(int) data["Super"];
        this.Description=data["Description"] as string;
        this.AccountType=(int) data["AccountType"];
        
        return 0;
    }
    
    public int saveChanges() {
        sql="UPDATE Positions SET Title='"+this.Title+"',Description='"+this.Description+"',AccountType="+this.AccountType+" WHERE ID="+this.ID;
        SqlDataReader data=execQuery(sql);
        
        this.closeCNN();
        
        return 0;
    }
    
    public int setTitle(string Title){
        if(Title.Length<256){
            this.Title=Title;
            return 0;
        } else {
            return 1;
        }
    }
    
    public int setDescription(string Description){
        if(Title.Length<2000){
            this.Description=Description;
            return 0;
        } else {
            return 1;
        }
    }
    
    public int setAccountType(int AccountType){
        if(AccountType<=3 && AccountType>0){
            this.AccountType=AccountType;
            return 0;
        } else {
            return 1;
        }
    }
    
    public int getSuperID(){
        return this.Super;
    }
    
    public PositionC getSuper(){
        return new PositionC(getSuperID());
    }
    
    public int getID(){
        return ID;
    }
    
    public string getTitle() {
        return Title;
    }
    
    public string getDescription() {
        return Description;
    }
    
    public int getAccountType() {
        return this.AccountType;
    }

    public string getAccountTypeString() {
        if(getAccountType()==1){
            return "Super Admin";
        } else if(getAccountType()==2){
            return "User Admin";
        } else if(getAccountType()==3){
            return "Standard User";
        }
        
        return "";
    }

    public string getDescriptionHtml() {
        if (getDescription()==null) {
            return "<span class=NoDesc>None</span>";
        } else {
            return "<span>"+getDescription()+"</span>";
        }
    }

}
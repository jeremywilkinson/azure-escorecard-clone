using System;
using System.Data.SqlClient;

public class AuditTemplateC : Item {

    int ID;
    string Name;
    DateTime DateCreated;
    int CreatorID;
    bool isFollowUp;

    public AuditTemplateC(int ID) : base(){
        load(ID);
    }

    public int load(int ID){
        sql="SELECT * FROM AuditTemplates WHERE ID="+ID;
        SqlDataReader data=execQuery(sql);
        data.Read();
        this.ID=ID;
        this.Name=(string) data["Name"];
        this.DateCreated=(DateTime) data["DateCreated"];
        this.CreatorID=(int) data["CreatorID"];
        this.isFollowUp=(bool) data["isFollowUp"];
        
        this.closeCNN();
        
        return 0;
    }

    public string getName(){
        return Name;
    }

}
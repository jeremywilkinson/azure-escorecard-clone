using System.Data.SqlClient;
using System;


public class Item : IDisposable{

    string connetionString = null;
    SqlConnection cnn;
    public string sql;

    public Item(){
        initDB();
    }
    
    void initDB(){
        int connectionTimeoutSeconds = 30;  // Default of 15 seconds is too short over the Internet, sometimes.
        int maxCountTriesConnectAndQuery = 3;  // You can adjust the various retry count values.
        int secondsBetweenRetries = 4;  // Simple retry strategy.

        // [A.1] Prepare the connection string to Azure SQL Database.
        SqlConnectionStringBuilder scsBuilder = new SqlConnectionStringBuilder();
            
        // Change these values to your values.
        scsBuilder["Server"] = "tcp:br5dwzopxj.database.windows.net/";
        scsBuilder["User ID"] = "twtraining@br5dwzopxj";  // @yourservername suffix sometimes.
        scsBuilder["Password"] = "@l3xchr15";
        scsBuilder["Database"] = "eScorecard";
        // Leave these values as they are.
        scsBuilder["Trusted_Connection"] = false;
        scsBuilder["Integrated Security"] = false;
        scsBuilder["Encrypt"] = true;
        scsBuilder["Connection Timeout"] = connectionTimeoutSeconds;

        connetionString = scsBuilder.ToString();// "Server=tcp:kznb9b607a.database.windows.net,1433;Database=eScorecard;User ID=TastewiseAdmin@kznb9b607a;Password={Tastewise1957};Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
    }
    
    public SqlDataReader execQuery(string SQL) {
        //do stuff
        cnn = new SqlConnection(connetionString);
        cnn.Open();
        using(SqlCommand command = new SqlCommand(SQL, cnn)){
            return command.ExecuteReader();
        }
    }
    
    public string formatDate(DateTime dt){
        return dt.ToString("dd/mm/yyyy");
    }
    
    public string formatDateTime(DateTime dt){
        return dt.ToString("dd/mm/yyyy HH:mm");
    }
    
    public void closeCNN(){
        cnn.Dispose();
    }
    
    void IDisposable.Dispose() {
        cnn.Dispose();
    }
    
    public string getConnectionString(){
        return connetionString;
    }
    
    public string Normalize(decimal value) {
        return (value/1.000000000000000000000000000000000m).ToString();;
    }

}
using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;

public class PKPATemplateC : Item {

    int ID;
    string Name;
    string Policy;

    public PKPATemplateC(int ID) : base(){
        load(ID);
    }

    public int load(int ID){
        sql="SELECT * FROM KPATemplates WHERE isParent=1 AND ID="+ID;
        SqlDataReader data=execQuery(sql);
        data.Read();
        this.ID=ID;
        this.Name=(string) data["Name"];
        this.Policy=(string) data["Policy"];
        
        this.closeCNN();
        
        return 0;
    }
    
    public int saveChanges(){
        sql="UPDATE KPATemplates SET Name='"+this.Name+"', Policy='"+this.Policy+"' WHERE ID="+this.ID;
        execQuery(sql);
        return 0;
    }
    
    public int getID(){
        return ID;
    }
    
    public string getName(){
        return this.Name;
    }
    
    public string getPolicy(){
        return this.Policy;
    }
    
    public string getPolicyHtml(){
        if(this.getPolicy()==""){
            return "<span class=NoDesc>None</span>";
        } else {
            return "<span>"+getPolicy()+"</span>";
        }
    }
    
    public int setName(string Name){
        if (Name.Length<255){
            this.Name=Name;
            return 0;
        }
        return 1;
    }

    public int setPolicy(string Policy){
        if (Policy.Length<2000){
            this.Policy=Policy;
            return 0;
        }
        return 1;
    }
    
    public PositionC[] getSuperPositions(){
        List<PositionC> Positions = new List<PositionC>();
        sql="SELECT Position FROM KPASuperLinks WHERE KPATemplate="+this.getID();
        SqlDataReader data = execQuery(sql);
            
        while (data.Read()){
            Positions.Add(new PositionC((int) data["Position"]));
        }
    
        return Positions.ToArray();
    }

}
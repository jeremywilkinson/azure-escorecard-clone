using System.Data.SqlClient;

public class CKPAC : Item {

    int ID;
    int Type;
    int Parent;
    decimal Weighting;
    CKPATemplateC CKPATemplate;

    public CKPAC(int ID) : base(){
        load(ID);
    }
    
    public int load(int ID){
        sql="SELECT * FROM KPAs LEFT JOIN KPATemplates ON KPAs.Type=KPATemplates.ID WHERE isParent=0 AND KPAs.ID="+ID;
        SqlDataReader data=execQuery(sql);
        data.Read();
        this.ID=ID;
        this.Parent=(int) data["Parent"];
        this.Weighting=(decimal) data["Weighting"];
        this.Type=(int) data["Type"];
        loadTemplate(Type);
        
        this.closeCNN();
        
        return 0;
    }
    
    public int getID(){
        return this.ID;
    }
    
    public string getName(){
        return this.getTemplate().getName();
    }
    
    public decimal getWeighting(){
        return this.Weighting*100;
    }
    
    public int loadTemplate(int ID){
        CKPATemplate = new CKPATemplateC(ID);
        
        return 0;
    }
    
    public CKPATemplateC getTemplate(){
        return this.CKPATemplate;
    }
    
    public PKPAC getParentPKPA(){
        return new PKPAC(this.Parent);
    }
    
    public ServiceAreaC getParentSA(){
        return this.getParentPKPA().getParentSA();
    }
    
    public StoreC getParentStore(){
        return this.getParentSA().getParentStore();
    }

    public string Print(decimal Score){
        return "<tr><td class=\"left CKPA\">Child KPA: "+this.getName()+"</td><td class=acceptableRange></td><td class=\"right\">"+Score+"%</td></tr>";
    }

}
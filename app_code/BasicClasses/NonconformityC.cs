using System.Data.SqlClient;
using System;

public class NonconformityC : Item {

    int ID;
    int CompletedAuditField;
    int? FollowUpAudit;
    string ActionPlan;
    bool Resolved;

    public NonconformityC(int ID) : base(){
        load(ID);
    }
    
    public NonconformityC() : base(){
        this.Resolved=false;
    }
    
    public int load(int ID){
        sql="SELECT * FROM Nonconformities WHERE ID="+ID;
        SqlDataReader data=execQuery(sql);
        data.Read();
        this.ID=ID;
        this.CompletedAuditField=(int) data["CompletedAuditField"];
        this.FollowUpAudit=data["FollowUpAudit"] as int?;;
        this.ActionPlan=data["ActionPlan"] as string;
        this.Resolved=Convert.ToBoolean(data["Resolved"]);
        
        this.closeCNN();
        
        return 0;
    }
    
    public AuditC getOriginalAudit(){
        return  getCompletedAuditField().getAudit();
    }
    
    public CompletedAuditFieldC getCompletedAuditField(){
        return new CompletedAuditFieldC(CompletedAuditField);
    }
    
    public int getCompletedAuditFieldID(){
        return this.CompletedAuditField;
    }
    
    public string getActionPlan(){
        return ActionPlan;
    }
    
    public string getActionPlanHtml(){
        string actionplan=getActionPlan();
        if (actionplan=="") {
            return "<span class=noDesc>None</span>";
        } else {
            return "<span>"+actionplan+"</span>";
        }
    }
    
    public void setCompletedAuditField(int ID){
        this.CompletedAuditField=ID;
    }

    public void setActionPlan(string ActionPlan){
        this.ActionPlan=ActionPlan;
    }

    public void setFollowUpAudit(int? ID){
        this.FollowUpAudit=ID;
    }
    
    public int? getFollowUpAuditID(){
        return this.FollowUpAudit;
    }
    
    public int saveNew(){
        if(this.getFollowUpAuditID()==null){
            sql="INSERT INTO Nonconformities (CompletedAuditField, ActionPlan, FollowUpAudit) VALUES ("+this.getCompletedAuditFieldID()+",'"+this.getActionPlan()+"',NULL)";
        } else {
            sql="INSERT INTO Nonconformities (CompletedAuditField, ActionPlan, FollowUpAudit) VALUES ("+this.getCompletedAuditFieldID()+",'"+this.getActionPlan()+"',"+this.getFollowUpAuditID()+")";
        }
        
        execQuery(sql);
        
        return 0;
    }
    
    public string Print(){
        return "<span class=\"apContainer\"><img src=\"http://tastewisescorecard.azurewebsites.net/media/AP.png\" class=\"ap\">: <span class=\"apWrapper\"\">\""+getActionPlan()+"\"</span></span>";
    }

}
using System.Data.SqlClient;

public class PKPAC : Item {

    int ID;
    int Type;
    int Parent;
    decimal Weighting;
    PKPATemplateC PKPATemplate;

    public PKPAC(int ID) : base(){
        load(ID);
    }
    
    public int load(int ID){
        sql="SELECT * FROM KPAs LEFT JOIN KPATemplates ON KPAs.Type=KPATemplates.ID WHERE isParent=1 AND KPAs.ID="+ID;
        SqlDataReader data=execQuery(sql);
        data.Read();
        this.ID=ID;
        this.Parent=(int) data["Parent"];
        this.Weighting=(decimal) data["Weighting"];
        this.Type=(int) data["Type"];
        loadTemplate(Type);
        
        this.closeCNN();
        
        return 0;
    }
    
    public int loadTemplate(int ID){
        PKPATemplate = new PKPATemplateC(ID);
        
        return 0;
    }
    
    public int getID(){
        return ID;
    }
    
    public string getName(){
        return this.getTemplate().getName();
    }
    
    public decimal getWeighting(){
        return this.Weighting*100;
    }
    
    public PKPATemplateC getTemplate(){
        return PKPATemplate;
    }
    
    public int getType(){
        return this.Type;
    }
    
    public ServiceAreaC getParentSA(){
        return new ServiceAreaC(this.Parent);
    }
    
    public StoreC getParentStore(){
        return this.getParentSA().getParentStore();
    }
    
    public PositionC[] getSuperPositions(){
        return this.PKPATemplate.getSuperPositions();
    }
    
    public EmployeeC[] getSupers(){
        EmployeeGenerator gen = new EmployeeGenerator();
        
        return gen.genByPositionsAndStore(getSuperPositions(), getParentStore());
    }
    
    public string Print(decimal Score){
        return "<tr><td class=\"left PKPA\">Parent KPA: "+this.getName()+"</td><td class=acceptableRange></td><td class=\"right\">"+Score+"%</td></tr>";
    }

}
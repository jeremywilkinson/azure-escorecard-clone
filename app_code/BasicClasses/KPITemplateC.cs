using System;
using System.Data.SqlClient;
using System.Collections.Generic;

public class KPITemplateC : Item {

    int ID;
    string Name;
    int InputType;
    decimal RangeMin;
    decimal RangeMax;
    bool Boolean;
    int Rating;
    string Description;
    string Units;

    public KPITemplateC(int ID) : base(){
        load(ID);
    }

    public int load(int ID){
        sql="SELECT * FROM KPITemplates WHERE ID="+ID;
        using (SqlDataReader data=execQuery(sql)) {
            data.Read();
            this.ID=ID;
            this.Name=(string) data["Name"];
            this.InputType=(int) data["InputType"];
            if (InputType==1) {
                this.RangeMin=(decimal)data["RangeMin"];
                this.RangeMax=(decimal)data["RangeMax"];
                this.Units=(string) data["Units"];
            } else if (InputType==2) {
                this.Boolean=(bool) data["Boolean"];
            } else if (InputType==3){
                this.Rating=(int) data["Rating"];
            } else if (InputType==4) {
                this.RangeMin=(decimal)data["RangeMin"];
                this.RangeMax=(decimal)data["RangeMax"];
            }
            this.Description=(string) data["Description"];
        }
        
        this.closeCNN();
        
        return 0;
    }
    
    public int getType(){
        return this.InputType;
    }
    
    public string getUnits(){
        return this.Units;
    }
    
    public string getName(){
        return this.Name;
    }
    
    public string getAcceptableRangeString(){
        if(this.InputType==1){
            return this.Normalize(this.RangeMin)+" to "+this.Normalize(RangeMax)+getUnits();
        } else if(this.InputType==2) {
            return this.Boolean.ToString();
        } else if(this.InputType==3) {
            return "Greater Than "+this.Rating.ToString();
        } else {
            return this.Normalize(this.RangeMin)+" to "+this.Normalize(RangeMax);
        }
    }
    
    public PositionC[] getResponsiblePositions() {
        List<PositionC> Positions = new List<PositionC>();
        sql="SELECT * FROM Positions WHERE ID IN (SELECT Position AS ID FROM KPISuperLinks WHERE KPITemplate="+ID+")";
        SqlDataReader data=execQuery(sql);
        while(data.Read()) {
            Positions.Add(new PositionC(data));
        }
        return Positions.ToArray();
    }
    
    public bool isComplient(decimal input){
        if(this.getType()==1){
            if(input>=this.RangeMin && input<=this.RangeMax){
                return true;
            } else {
                return false;
            }
        } else if(this.getType()==2){
            if(Convert.ToBoolean(input)==this.Boolean){
                return true;
            } else {
                return false;
            }
        } else if(this.getType()==3){
            if(input>=this.Rating){
                return true;
            } else {
                return false;
            }
        } else if(this.getType()==4){
            if(input>=this.RangeMin && input<=this.RangeMax){
                return true;
            } else {
                return false;
            }
        }
    
        return false;
    }

}
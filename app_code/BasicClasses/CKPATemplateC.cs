using System;
using System.Data.SqlClient;

public class CKPATemplateC : Item {

    int ID;
    string Name;
    string Policy;

    public CKPATemplateC(int ID) : base(){
        load(ID);
    }

    public int load(int ID){
        sql="SELECT * FROM KPATemplates WHERE isParent=0 AND ID="+ID;
        SqlDataReader data=execQuery(sql);
        data.Read();
        this.ID=ID;
        this.Name=(string) data["Name"];
        this.Policy=(string) data["Policy"];
        
        this.closeCNN();
        
        return 0;
    }
    
    public string getName(){
        return this.Name;
    }

}
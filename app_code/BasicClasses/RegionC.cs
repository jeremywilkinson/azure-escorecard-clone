using System.Data.SqlClient;

public class RegionC : Item {

    int ID;
    string Name;
    int? Super;
    string Location;
    string Description;

    public RegionC(int ID) : base(){
        load(ID);
    }
    
    public RegionC(SqlDataReader data) : base(){
        load(data);
    }
    
    public int load(int ID){
        sql="SELECT * FROM Regions WHERE ID="+ID;
        SqlDataReader data=execQuery(sql);
        data.Read();
        this.ID=ID;
        this.Name=data["Name"] as string;
        this.Super=data["Super"] as int?;
        this.Location=data["Location"] as string;
        this.Description=data["Description"] as string;
        
        this.closeCNN();
        
        return 0;
    }
    
    public int load(SqlDataReader data){
        
        this.ID=(int) data["ID"];
        this.Name=data["Name"] as string;
        this.Super=data["Super"] as int?;
        this.Location=data["Location"] as string;
        this.Description=data["Description"] as string;
        
        return 0;
    }
    
    public int saveChanges() {
        sql="UPDATE Regions SET Name='"+this.Name+"', Location='"+this.Location+"', Description='"+this.Description+"' WHERE ID="+this.ID;
        execQuery(sql);
        return 0;
    }
    
    public int getID() {
        return ID;
    }
    
    public string getName() {
        return Name;
    }
    
    public string getLocation() {
        return Location;
    }
    
    public string getDescription() {
        return Description;
    }
    
    public int setName(string Name){
        if (Name.Length<=255){
            this.Name=Name;
            return 0;
        }
        return 1;
    }
    
    public int setLocation(string Location){
        if (Location.Length<=255){
            this.Location=Location;
            return 0;
        }
        return 1;
    }

    public int setDescription(string Description){
        if (Description.Length<=2000){
            this.Description=Description;
            return 0;
        }
        return 1;
    }
    
}
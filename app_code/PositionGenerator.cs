using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;

public class PositionGenerator : Item {

    public PositionGenerator() : base(){
    }

    public PositionC[] genAll(){
        List<PositionC> Positions = new List<PositionC>();
        
        sql="SELECT * FROM Positions";
        SqlDataReader data = execQuery(sql);
        
        while (data.Read()){
            Positions.Add(new PositionC(data));
        }
        
        return Positions.ToArray();
    }

    public bool Exists(int ID){
        sql="SELECT COUNT(ID) AS num FROM Positions WHERE ID="+ID;
        SqlDataReader data = execQuery(sql);
        data.Read();
        
        if((int) data["num"]!=0){
            return true;
        }
        return false;
    }

}
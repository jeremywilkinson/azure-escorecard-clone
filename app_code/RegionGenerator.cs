using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;

public class RegionGenerator : Item {

    public RegionGenerator() : base(){
    }

    public RegionC[] genAll(){
        List<RegionC> Regions = new List<RegionC>();
        
        sql="SELECT * FROM Regions";
        SqlDataReader data = execQuery(sql);
        
        while (data.Read()){
            Regions.Add(new RegionC(data));
        }
        
        return Regions.ToArray();
    }
    
    public bool Exists(int ID){
        sql="SELECT COUNT(ID) AS num FROM Regions WHERE ID="+ID;
        SqlDataReader data = execQuery(sql);
        data.Read();
        
        if((int) data["num"]!=0){
            return true;
        }
        return false;
    }

}
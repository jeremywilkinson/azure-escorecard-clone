﻿public class Block {

    int pos;
    int scale;
    string text;
    int id;
    bool selected;

    public Block(int pos, int scale, string text, int id, bool selected){
        this.pos=pos;
        this.scale=scale;
        this.text=text;
        this.id=id;
        this.selected=selected;
    }

    public string GetHtml(){

        string check;

        if(selected){
            check="check";
        } else {
            check="unchecked";
        }

        int pxpos=(scale+5)*pos + 5;

        return "<div style=\"position: absolute;left:"+pxpos+"px\" class=\"block\">"+text+"<div class=\"check\"><span class=\"glyphicon glyphicon-"+check+"\"></span></div></div>";

    }

}
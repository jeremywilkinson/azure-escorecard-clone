using System.Net;
using System.Web.UI.WebControls;
using System.Net.Mail;

public class EmailSender {
    
    MailAddress fromAddress = new MailAddress("feedback.escorecard@gmail.com","Tastewise Scorecard");
    string fromPassword = "Tastewise1957";
    
    SmtpClient smtp;
    
    public EmailSender() {
        smtp = new SmtpClient
    { 
        Host = "smtp.gmail.com",
        Port = 587,
        EnableSsl = true,
        DeliveryMethod = SmtpDeliveryMethod.Network,
        UseDefaultCredentials = false,
        Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
    };
    }
    
    public void send(string email, string subject, string recipient) {
        MailAddress toAddress = new MailAddress(recipient,"");
        using (var message = new MailMessage(fromAddress, toAddress) {
                Subject = subject,
                Body = email,
                IsBodyHtml = true
            }) {
            smtp.Send(message);
        }
    }
}
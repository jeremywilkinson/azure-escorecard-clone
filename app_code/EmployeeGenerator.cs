using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;

public class EmployeeGenerator : Item {

    public EmployeeGenerator() : base(){
    }

    public EmployeeC[] genAll(){
        List<EmployeeC> Employees = new List<EmployeeC>();
        
        sql="SELECT * FROM Employees";
        SqlDataReader data = execQuery(sql);
        
        while (data.Read()){
            Employees.Add(new EmployeeC(data));
        }
        
        return Employees.ToArray();
    }
    
    public EmployeeC[] genByPositionsAndStore(PositionC[] Positions, StoreC Store){
        if (Positions.Length>0) {
            List<EmployeeC> Employees = new List<EmployeeC>();
            string posarray = "("+String.Join(",",from pos in Positions select pos.getID().ToString())+")";
            
            sql="SELECT * FROM Employees WHERE Store="+Store.getID()+" AND Position IN "+posarray;
            SqlDataReader data = execQuery(sql);
            
            while (data.Read()){
                Employees.Add(new EmployeeC(data));
            }
            
            return Employees.ToArray();
        }
        return new EmployeeC[0];
    }

    public bool Exists(int ID){
        sql="SELECT COUNT(ID) AS num FROM Employees WHERE ID="+ID;
        SqlDataReader data = execQuery(sql);
        data.Read();
        
        if((int) data["num"]!=0){
            return true;
        }
        return false;
    }

}
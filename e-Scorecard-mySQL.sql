DROP DATABASE eScorecard;

#Removes Previous Instance WARNING, WILL EREASE ALL DATA

CREATE DATABASE eScorecard;

#Create db

CREATE TABLE eScorecard.Positions
(
    ID int AUTO_INCREMENT,
    Title varchar(255) UNIQUE,
    Super int,
    Description varchar(2000),
    PRIMARY KEY (ID),
    FOREIGN KEY (Super) REFERENCES Positions(ID)
);
 #Creates Positions Table

INSERT INTO eScorecard.Positions (Title, Super, Description) VALUES ("Admin", 1, "System Admin");# 1 row affected.



CREATE TABLE eScorecard.Employees
(
    ID int AUTO_INCREMENT,
    Position int,
    Super int,
    LastName varchar(255),
    FirstName varchar(255),
    Username varchar(255) UNIQUE,
    Password varchar(64),
    ContactNumber varchar(10),
    Image varchar(100),
    PRIMARY KEY (ID),
    FOREIGN KEY (Position) REFERENCES Positions(ID),
    FOREIGN KEY (Super) REFERENCES Employees(ID)
);
 #Creates Employees Table

INSERT INTO eScorecard.Employees (Position, Super, LastName, FirstName, Username, Password, ContactNumber, Image) VALUES (1, 1, "", "Admin", "Admin", "4e7afebcfbae000b22c7c85e5560f89a2a0280b4", "0987654321", "1.jpg");# 1 row affected.

#Inserts the admin

CREATE TABLE eScorecard.Regions
(
    ID int AUTO_INCREMENT,
    Name varchar(255) UNIQUE,
    Super int,
    Location varchar(2000),
    Description varchar(2000),
    PRIMARY KEY (ID),
    FOREIGN KEY (Super) REFERENCES Employees(ID)
);



CREATE TABLE eScorecard.Stores
(
    ID int AUTO_INCREMENT,
    Name varchar(255) UNIQUE,
    Super int,
    Region int,
    Address varchar(500),
    ContactNumber varchar(10),
    PRIMARY KEY (ID),
    FOREIGN KEY (Super) REFERENCES Employees(ID),
    FOREIGN KEY (Region) REFERENCES Regions(ID)
);



CREATE TABLE eScorecard.ServiceAreaTemplates
(
    ID int AUTO_INCREMENT,
    Name varchar(255) UNIQUE,
    Policy varchar(2000),
    PRIMARY KEY (ID)
);



CREATE TABLE eScorecard.ServiceAreas
(
    ID int AUTO_INCREMENT,
    Type int,
    Parent int,
    Weighting float,
    PRIMARY KEY (ID),
    FOREIGN KEY (Type) REFERENCES ServiceAreaTemplates(ID),
    FOREIGN KEY (Parent) REFERENCES Stores(ID)
);



CREATE TABLE eScorecard.KPATemplates
(
    ID int AUTO_INCREMENT,
    Name varchar(100),
    Policy varchar(1000),
    isParent bool,
    PRIMARY KEY (ID)
);



CREATE TABLE eScorecard.KPAs
(
    ID int AUTO_INCREMENT,
    Type int,
    Parent int,
    Weighting float,
    PRIMARY KEY (ID),
    FOREIGN KEY (Type) REFERENCES KPATemplates(ID),
    FOREIGN KEY (Parent) REFERENCES ServiceAreaTemplates(ID)
);



CREATE TABLE eScorecard.KPITemplates
(
    ID int AUTO_INCREMENT,
    Name varchar(100),
    InputType int,
    RangeMin int,
    RangeMax int,
    Boolean bool,
    Rating int,
    Description varchar(1000),
    PRIMARY KEY (ID)
);



CREATE TABLE eScorecard.KPIs
(
    ID int AUTO_INCREMENT,
    Type int,
    Parent int,
    Weighting float,
    PRIMARY KEY (ID),
    FOREIGN KEY (Type) REFERENCES KPITemplates(ID),
    FOREIGN KEY (Parent) REFERENCES KPATemplates(ID)
);



CREATE TABLE eScorecard.AuditTemplates
(
    ID int AUTO_INCREMENT,
    Name varchar(500),
    DateCreated Date,
    CreatorID int,
    isFollowUp bool,
    PRIMARY KEY (ID),
    FOREIGN KEY (CreatorID) REFERENCES Employees(ID)
);


CREATE TABLE eScorecard.Audits
(
    ID int AUTO_INCREMENT,
    DateScheduled DATETIME,
    Type int,
    Super int,
    Store int,
    PRIMARY KEY (ID),
    FOREIGN KEY (Super) REFERENCES Employees(ID),
    FOREIGN KEY (Type) REFERENCES AuditTemplates(ID)
);


CREATE TABLE eScorecard.AuditFields
(
    ID int AUTO_INCREMENT,
    Audit int,
    Type int,
    ServiceArea int,
    ParentKPA int,
    ChildKPA int,
    PRIMARY KEY (ID),
    FOREIGN KEY (Audit) REFERENCES AuditTemplates(ID),
    FOREIGN KEY (Type) REFERENCES KPITemplates(ID),
    FOREIGN KEY (ServiceArea) REFERENCES ServiceAreaTemplates(ID),
    FOREIGN KEY (ParentKPA) REFERENCES KPATemplates(ID),
    FOREIGN KEY (ChildKPA) REFERENCES KPATemplates(ID)
);

